require essioc

epicsEnvSet("ENGINEER", "{{ cookiecutter.full_name }} <{{ cookiecutter.email }}>"

iocshLoad("$(essioc_DIR)/common_config.iocsh")
